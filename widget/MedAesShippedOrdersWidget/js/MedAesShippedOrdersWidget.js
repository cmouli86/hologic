/**
 * @fileoverview Footer Widget.
 *
 * @author Taistech
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  // Adding knockout
  //-------------------------------------------------------------------
  ['knockout', 'ccResourceLoader!global/api-helper', 'notifier',
      'navigation', 'pageLayout/cart', 'ccRestClient', 'ccConstants', 'pubsub', 'viewModels/multiCartViewModel','spinner'
  ],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, helper, notifier, navigation, pageLayout, ccRestClient, CCConstants, pubsub, MultiCartViewModel,spinner) {

      "use strict";


      return {

          showShippedOrders: ko.observableArray([]),
          selectedOrderId: ko.observable(),
          getOrderLevelItems: ko.observable(),
          getCurrentProductData: ko.observableArray([]),
          onLoad: function(widget) {
              // price complete call
              $.Topic(pubsub.topicNames.CART_PRICE_COMPLETE).subscribe(function(obj) {
                  if (window.location.pathname.toLowerCase().indexOf('home') !== -1 || window.location.pathname === "") {
                     // console.log('its coming inside the price complete....');
                      widget.getCurrentProductData().shift();
                      if (widget.getCurrentProductData().length > 0) {
                          if (widget.getCurrentProductData().length !== 1) {
                              widget.cart().addItem(ko.toJS(widget.getCurrentProductData()[0]));
                              $("html, body").animate({scrollTop: 0}, "slow");
                          } else {
                              $.Topic(pubsub.topicNames.CART_ADD).publishWith(
                                  ko.toJS(widget.getCurrentProductData()[0]), [{
                                      message: "success"
                                  }]);
                              widget.destroySpinner();
                              $("html, body").animate({scrollTop: 0}, "slow");
                          }
                      }
                  }
              });


          },
          externalShippingOrderCall: function() {
              var self = this;
              var getDate = new Date();
              var getYearVal = getDate.getFullYear();
              var getMonth = getDate.getMonth() + 1;
              var getMonthVal = getMonth < 10 ? '0' + getMonth : getMonth;
              var getDateVal = getDate.getDate() < 10 ? '0' + getDate.getDate() : getDate.getDate();
              var toDate = getYearVal + '-' + getMonthVal + '-' + getDateVal;
              var getFirstDate = new Date(getDate.getFullYear(), getDate.getMonth(), 1);
              var month = getFirstDate.getMonth() < 10 ? '0' + getFirstDate.getMonth() : getFirstDate.getMonth();
              var fromDate = getFirstDate.getFullYear() + '-' + month + '-0' + getFirstDate.getDate();
              var getData = helper.apiEndPoint.orderHistory + '?siteURL=' + self.site().extensionSiteSettings.externalSiteSettings.siteUrl + '&siteName=' + self.site().extensionSiteSettings.externalSiteSettings.siteName + '&accountId=' + self.user().currentOrganization().repositoryId + '&status=SHIPPED' + "&fromDate=" + fromDate + "&toDate=" + toDate;
              helper.getDataExternal(getData, function(err, result) {
                  if (result && result.orders) {
                      var showShipped = result.orders.slice(0, 3);
                      for(var i=0; i<showShipped.length;i++) {
                          if(showShipped[i].shipments[0].shipper == "FEDEX" || showShipped[i].shipments[0].shipper == "FDXG") {
                              showShipped[i].shipments[0].trackingUrl = (showShipped[i].shipments[0].waybill) ? helper.apiEndPoint.trackFedEx + showShipped[i].shipments[0].waybill : "";
                          } else {
                            showShipped[i].shipments[0].trackingUrl = "";
                        }
                      }
                      self.showShippedOrders(showShipped);
                  }
              })
          },
          redirectOrderPage: function(orderid) {
              console.log("orderId", orderid);
              navigation.goTo("/OrderDetailsPage?orderNo=" + orderid);

          },
          mergeWithParticularIncompleteOrder: function(pOrderId) {
              var widget = this;
             // widget.createSpinner();
              /** Change the pOrderID in the selectedOrderId, Once the Order Id issue is fixed **/
              widget.selectedOrderId(pOrderId);
              widget.cart().mergeCart(true);
              ccRestClient.request(CCConstants.ENDPOINT_GET_ORDER, null,
                  function(order) {
                      var state = order.state;
                      var success = function() {
                          //widget.user().validateAndRedirectPage("/cart");
                      };
                      var error = function(errorBlock) {
                          var errMessages = "";
                          var displayName;
                          for (var k = 0; k < errorBlock.length; k++) {
                              errMessages = errMessages + "\r\n" + errorBlock[k].errorMessage;
                          }
                          notifier.sendError("CartViewModel", "One or more items on this order may not be available to reorder so this functionality is unavailable at this time.", true);
                      };     

                      widget.getOrderLevelItems(order.order.items);
                      var getProductIds = [];
                      for (var i = 0; i < widget.getOrderLevelItems().length; i++) {
                          getProductIds.push(widget.getOrderLevelItems()[i].productId);
                      }
                      if (getProductIds.length > 0) {
                          widget.getProductData(getProductIds);

                      }

                  },
                  function(err) {
                      console.log("error", err);
                      widget.cart().mergeCart(false);
                      widget.selectedOrderId(null);
                      notifier.sendError("CartViewModel", "One or more items on this order may not be available to reorder so this functionality is unavailable at this time.", true);
                  },
                  widget.selectedOrderId());
          },

          getProductData: function(getIds) {
              var widget = this;
              var requestData = {};
              ccRestClient.request("/ccstoreui/v1/products?productIds=" + getIds.toString(), {}, function(data) {
                  if (data) {
                      widget.updateData(data);
                  }
              }, function(err) {
                  //  console.log(err)
              }, "GET");


          },
          updateData: function(getData) {
              var widget = this;
              ko.mapping.fromJS(getData, {}, this.getCurrentProductData);
              for (var i = 0; i < this.getCurrentProductData().length; i++) {
                  for (var j = 0; j < widget.getOrderLevelItems().length; j++) {
                      if (widget.getCurrentProductData()[i].id() == widget.getOrderLevelItems()[j].productId) {
                          widget.getCurrentProductData()[i].orderQuantity = parseInt(1, 10);
                          widget.getCurrentProductData()[i].externalPrice = widget.getOrderLevelItems()[j].externalPrice;
                          widget.getCurrentProductData()[i].externalPriceQuantity = widget.getOrderLevelItems()[j].externalPriceQuantity;
                          //console.log( widget.getCurrentProductData()[i].orderQuantity,"... widget.getCurrentProductData()[i].orderQuantity....");
                      }
                  }
              }

              if (widget.getCurrentProductData().length > 1) {
                  widget.cart().addItem(ko.toJS(widget.getCurrentProductData()[0]));
              } else {
                  $.Topic(pubsub.topicNames.CART_ADD).publishWith(
                      ko.toJS(widget.getCurrentProductData()[0]), [{
                          message: "success"
                      }]);
                 widget.destroySpinner();
                $("html, body").animate({scrollTop: 0}, "slow");
              }

          },
          beforeAppear: function(page) {
              var widget = this;
              widget.externalShippingOrderCall();



          },
          /**
           * Destroy the 'loading' spinner.
           * @function  OrderViewModel.destroySpinner
           */
          destroySpinner: function() {
              // console.log("destroyed");
              $('#loadingModal').hide();
              spinner.destroy();
          },

          /**
           * Create the 'loading' spinner.
           * @function  OrderViewModel.createSpinner
           */
          createSpinner: function(loadingText) {
              var indicatorOptions = {
                  parent: '#loadingModal',
                  posTop: '0',
                  posLeft: '50%'
              };
            //  var loadingText = CCi18n.t('ns.common:resources.loadingText');
              $('#loadingModal').removeClass('hide');
              $('#loadingModal').show();
              indicatorOptions.loadingText = loadingText;
              spinner.create(indicatorOptions);
          },

      };
  }
);